class Discount < ApplicationRecord
  enum status: [:Ativo, :Desativo]
  enum kind: [:porcentagem, :dinheiro]
  has_many :sales
end
