class Product < ApplicationRecord
  validates :name, :status, :price, :store_id, presence: :true
  enum status: [:Ativo, :Desativo]
  has_many :product_quantities
  mount_uploader :photo, PhotoUploader
  belongs_to :store
end
