class Goal < ApplicationRecord
  # validates :name, :date_start, :value, :date_finish, :store_id, presence: :true
  has_many :users
  belongs_to :store
end
