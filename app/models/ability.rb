class Ability
  include CanCan::Ability

  def initialize(user)
    if user
      if user.kind == 'Vendedor'
        can :access, :rails_admin
        can :dashboard
        can :manage, Client, user_id: user.id
        can :manage, Sale, user_id: user.id
        can :read, Product, status: :Ativo
        can :read, Discount, status: :Ativo
        can :read, Comission, user_id: user.id
        can :read, Goal, user_id: user.id
        can :manage, ProductQuantity, user_id: user.id
        can :manage, Address, user_id: user.id
      elsif user.kind == 'Proprietário'
        can :access, :rails_admin
        can :dashboard
        can :manage, Client, user_id: user.id
        can :manage, Sale, user_id: user.id
        can :read, Store, user_id: user.id
        can :manage, Product, store_id: user.id
        can :manage, Goal, user_id: user.id
        can :manage, Discount, status: :Ativo
        can :manage, Comission, user_id: user.id
        can :manage, ProductQuantity,user_id: user.id
        can :manage, Address, user_id: user.id
      elsif user.kind == 'Gerente'
        can :manage, :all
      end
    end
  end
end