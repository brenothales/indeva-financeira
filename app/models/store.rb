class Store < ApplicationRecord
    validates :name, :cnpj, :status, presence: :true

    enum status: [:Ativo, :Desativo]
    has_many :products
    has_many :users
    has_many :goals
end
