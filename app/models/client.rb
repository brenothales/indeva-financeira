class Client < ApplicationRecord
  belongs_to :user
  enum status: [:Ativo, :Desativo]
  has_one :address
end
