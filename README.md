# README

Um simples aplicação de gerenciamento de financeiro para a empresa Indeva, o sistema seguirá com
mais implmentações e correção de bugs.


Gerente -> Super Administrador
```
$ gerente@teste.com, senha: 123456
```

Proprietário -> Dono da loja
```
$ snow@gmail.com, senha: 123456
```
```
$ sandra@gmail.com, senha: 123456
```

Vendedor -> Funcionário
```
$ vendedor@teste.com, senha: 123456
```
```
$ vendedor2@teste.com, senha: 123456
```

## Demonstração
[Demo](https://indeva-breno.herokuapp.com/)