# Criando nossos Users --- OBS: Depois que adicionarmos o devise precisamos incluir o email e senha dos users
User.create name: 'José', status: :Ativo, kind: :Vendedor, email: 'vendedor@teste.com', password: 123456
User.create name: 'Manuel', status: :Ativo, kind: :Vendedor, email: 'vendedor2@teste.com', password: 123456
User.create name: 'Marcos', status: :Ativo, kind: :Gerente, email: 'gerente@teste.com', password: 123456

Store.create name: 'Loja A', cnpj: '212343-56/0001', status: :Ativo
Store.create name: 'Loja B', cnpj: '323235-42/0001', status: :Ativo
Store.create name: 'Loja C', cnpj: '676899-36/0001', status: :Ativo

# Criando alguns produtos de exemplo
Product.create name: 'Smartphone', description:'Um smartphone novo ...', status: :Ativo, price: 10, store_id: 2
Product.create name: 'Tablet', description:'Um tablet novo ...', status: :Ativo, price: 20, store_id: 1

# Criando um desconto de exemplo
Discount.create name: 'Desconto carnaval', description: 'Aplique esse desconto no carnaval', value: '10', kind: :porcentagem, status: :Ativo
Discount.create name: 'Desconto carnaval dinheiro', description: 'Aplique esse desconto quando possível', value: '10', kind: :dinheiro, status: :Ativo

# Crindo client
Client.create name: 'Paulo', company_name: 'Google', document: '1234', email: 'paulo@google.com', user: User.first
Client.create name: 'Julia', company_name: 'Google', document: 'abcd', email: 'julia@google.com', user: User.first

