class CreateStores < ActiveRecord::Migration[5.0]
  def change
    create_table :stores do |t|
      t.string :name
      t.string :cnpj
      t.integer :status

      t.timestamps
    end
  end
end
